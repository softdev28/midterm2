package com.thunwarat.midterm2;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ACER
 */
public class Midtrem2 {

    public static void main(String args[]) {
        Scanner kb = new Scanner(System.in);
        int c = kb.nextInt();
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < n; i++) {
            A[i] = kb.nextInt();
        }
        System.out.println(checkEqualsC(A, c));
    }

    public static boolean checkEqualsC(int[] A, int c) {
        for (int i = 0; i < A.length; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[i] + A[j] == c) {
                    return true;
                }
            }
        }
        return false;
    }
}
